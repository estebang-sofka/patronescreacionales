package co.com.sofka.estebang;

import co.com.sofka.estebang.patronAbstractFactory.plan.Plan;
import co.com.sofka.estebang.patronAbstractFactory.plan.PlanPostpago;
import co.com.sofka.estebang.patronAbstractFactory.plan.PlanPrepago;
import co.com.sofka.estebang.patronBuilder.builders.NotificationBuilder;
import co.com.sofka.estebang.patronBuilder.builders.NotificationLogBuilder;
import co.com.sofka.estebang.patronBuilder.director.Director;
import co.com.sofka.estebang.patronBuilder.model.Notification;
import co.com.sofka.estebang.patronBuilder.model.NotificationLog;
import co.com.sofka.estebang.patronSingleton.UnidadResidencial;

public class Main {

    public static void main(String[] args) {
        // Patrón Builder
        ejecutarPatronBuilder();

        // Patrón Factory Method
        ejecutarPatronFactoryMethod();

        // Patrón Singleton
        ejecutarPatronSingleton();
    }

    private static void ejecutarPatronSingleton() {
        UnidadResidencial unidadResidencial1 = UnidadResidencial.getInstance(6, true, false);
        UnidadResidencial unidadResidencial2 = UnidadResidencial.getInstance(10, false, true);
        System.out.println(unidadResidencial1);
        System.out.println(unidadResidencial2);
    }

    private static void ejecutarPatronFactoryMethod() {
        Plan plan;

        plan = new PlanPostpago(60000);
        plan.imprimirFactura();

        plan = new PlanPrepago(12000);
        plan.imprimirFactura();
    }

    private static void ejecutarPatronBuilder(){
        Director director = new Director();

        NotificationBuilder notificationBuilder = new NotificationBuilder();
        director.buildNotificationAPP(notificationBuilder);
        Notification notification = notificationBuilder.build();

        System.out.println("Notification built: " + notification.getType());

        NotificationLogBuilder notificationLogBuilder = new NotificationLogBuilder();
        director.buildNotificationAPP(notificationLogBuilder);
        NotificationLog notificationLog = notificationLogBuilder.build();

        System.out.println("Notification log built: " + notificationLog.toString());
    }
}
