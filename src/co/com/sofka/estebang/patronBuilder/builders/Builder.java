package co.com.sofka.estebang.patronBuilder.builders;

import co.com.sofka.estebang.patronBuilder.model.NotificationType;
import co.com.sofka.estebang.patronBuilder.model.Person;

import java.io.File;

public interface Builder {
    void setNotificationType(NotificationType type);
    void setContent(String content);
    void setTarget(Person target);
    void setTitle(String title);
    void setAttachment(File attachment);
    void setOrigin(Person origin);
}
