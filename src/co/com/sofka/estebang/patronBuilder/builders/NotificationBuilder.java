package co.com.sofka.estebang.patronBuilder.builders;

import co.com.sofka.estebang.patronBuilder.model.Notification;
import co.com.sofka.estebang.patronBuilder.model.NotificationType;
import co.com.sofka.estebang.patronBuilder.model.Person;

import java.io.File;

public class NotificationBuilder implements Builder {

    private NotificationType type;
    private String content;
    private Person target;
    private String title;
    private File attachment;
    private Person origin;

    @Override
    public void setNotificationType(NotificationType type) {
        this.type = type;
    }

    @Override
    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public void setTarget(Person target) {
        this.target = target;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void setAttachment(File attachment) {
        this.attachment = attachment;
    }

    @Override
    public void setOrigin(Person origin) {
        this.origin = origin;
    }

    public Notification build() {
        return new Notification(type, content, target, title, attachment, origin);
    }
}
