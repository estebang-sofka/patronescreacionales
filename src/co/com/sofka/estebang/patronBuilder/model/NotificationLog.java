package co.com.sofka.estebang.patronBuilder.model;

import java.io.File;

public class NotificationLog {
    private NotificationType type;
    private String content;
    private Person target;
    private String title;
    private File attachment;
    private Person origin;

    public NotificationLog(NotificationType type, String content, Person target, String title, File attachment, Person origin) {
        this.type = type;
        this.content = content;
        this.target = target;
        this.title = title;
        this.attachment = attachment;
        this.origin = origin;
    }

    public void print() {
        System.out.print(this);
    }

    @Override
    public String toString() {
        return "NotificationLog{" +
                "type=" + type +
                ", content='" + content + '\'' +
                ", target=" + target +
                ", title='" + title + '\'' +
                ", attachment=" + attachment +
                ", origin=" + origin +
                '}';
    }
}
