package co.com.sofka.estebang.patronBuilder.model;

import java.io.File;

public class Notification {
    private NotificationType type;
    private String content;
    private Person target;
    private String title;
    private File attachment;
    private Person origin;

    public Notification(NotificationType type, String content, Person target, String title, File attachment, Person origin) {
        this.type = type;
        this.content = content;
        this.target = target;
        this.title = title;
        this.attachment = attachment;
        this.origin = origin;
    }

    public NotificationType getType() {
        return type;
    }

    public String getContent() {
        return content;
    }

    public Person getTarget() {
        return target;
    }

    public String getTitle() {
        return title;
    }

    public File getAttachment() {
        return attachment;
    }

    public Person getOrigin() {
        return origin;
    }
}
