package co.com.sofka.estebang.patronBuilder.model;

public enum NotificationType {
    SMS, EMAIL, APP
}
