package co.com.sofka.estebang.patronBuilder.model;

public class Person {
    private String id;
    private String name;
    private String lastname;
    private String email;
    private String cellphoneNumber;

    public Person(String id, String name, String lastname, String email, String cellphoneNumber) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.cellphoneNumber = cellphoneNumber;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                '}';
    }
}
