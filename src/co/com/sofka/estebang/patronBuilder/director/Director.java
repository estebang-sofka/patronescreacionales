package co.com.sofka.estebang.patronBuilder.director;

import co.com.sofka.estebang.patronBuilder.builders.Builder;
import co.com.sofka.estebang.patronBuilder.model.NotificationType;
import co.com.sofka.estebang.patronBuilder.model.Person;

import java.io.File;

public class Director {

    public void buildNotificationSMS(Builder builder) {
        builder.setAttachment(new File(""));
        builder.setTarget(new Person("111", "Esteban", "García", "e.g@sofka.com.co", "1111111"));
        builder.setTitle("Hi");
        builder.setOrigin(new Person("222", "Raúl", "Alzate", "r.a@sofka.com.co", "2222222"));
        builder.setContent("Hi Raul via SMS");
        builder.setNotificationType(NotificationType.SMS);
    }

    public void buildNotificationAPP(Builder builder) {
        builder.setAttachment(new File(""));
        builder.setTarget(new Person("111", "Esteban", "García", "e.g@sofka.com.co", "1111111"));
        builder.setTitle("Hi");
        builder.setOrigin(new Person("222", "Raúl", "Alzate", "r.a@sofka.com.co", "2222222"));
        builder.setContent("Hi Raul via APP");
        builder.setNotificationType(NotificationType.APP);
    }

    public void buildNotificationEmail(Builder builder) {
        builder.setAttachment(new File(""));
        builder.setTarget(new Person("111", "Esteban", "García", "e.g@sofka.com.co", "1111111"));
        builder.setTitle("Hi");
        builder.setOrigin(new Person("222", "Raúl", "Alzate", "r.a@sofka.com.co", "2222222"));
        builder.setContent("Hi Raul via EMAIL");
        builder.setNotificationType(NotificationType.EMAIL);
    }
}
