package co.com.sofka.estebang.patronAbstractFactory.factura;

import co.com.sofka.estebang.patronAbstractFactory.plan.Plan;
import co.com.sofka.estebang.patronAbstractFactory.plan.PlanPrepago;

public class FacturaPrepago implements Factura {

    @Override
    public void calcularFactura(Plan plan) {
        System.out.println("Factura Prepago por un total de: " + ((PlanPrepago) plan).getValorPaquete());
    }
}
