package co.com.sofka.estebang.patronAbstractFactory.factura;

import co.com.sofka.estebang.patronAbstractFactory.plan.Plan;
import co.com.sofka.estebang.patronAbstractFactory.plan.PlanPostpago;

public class FacturaPostpago implements Factura {

    @Override
    public void calcularFactura(Plan plan) {
        System.out.println("Factura Postpago por un total de: " + ((PlanPostpago) plan).getMensualidad());
    }
}
