package co.com.sofka.estebang.patronAbstractFactory.factura;

import co.com.sofka.estebang.patronAbstractFactory.plan.Plan;

public interface Factura {

    void calcularFactura(Plan plan);
}
