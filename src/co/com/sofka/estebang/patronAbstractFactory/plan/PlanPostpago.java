package co.com.sofka.estebang.patronAbstractFactory.plan;

import co.com.sofka.estebang.patronAbstractFactory.factura.Factura;
import co.com.sofka.estebang.patronAbstractFactory.factura.FacturaPostpago;

public class PlanPostpago extends Plan {
    private int mensualidad;

    public PlanPostpago(int mensualidad) {
        super();
        this.mensualidad = mensualidad;
    }

    @Override
    public Factura crearFactura() {
        return new FacturaPostpago();
    }

    public int getMensualidad() {
        return mensualidad;
    }
}
