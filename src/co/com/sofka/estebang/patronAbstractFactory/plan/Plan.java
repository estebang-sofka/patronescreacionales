package co.com.sofka.estebang.patronAbstractFactory.plan;

import co.com.sofka.estebang.patronAbstractFactory.factura.Factura;

public abstract class Plan {
    public void imprimirFactura() {
        Factura factura = crearFactura();
        factura.calcularFactura(this);
    }

    public abstract Factura crearFactura();
}
