package co.com.sofka.estebang.patronAbstractFactory.plan;

import co.com.sofka.estebang.patronAbstractFactory.factura.Factura;
import co.com.sofka.estebang.patronAbstractFactory.factura.FacturaPrepago;

public class PlanPrepago extends Plan {
    private int valorPaquete;

    public PlanPrepago(int valorPaquete) {
        this.valorPaquete = valorPaquete;
    }

    @Override
    public Factura crearFactura() {
        return new FacturaPrepago();
    }

    public int getValorPaquete() {
        return valorPaquete;
    }
}
