package co.com.sofka.estebang.patronSingleton;

public final class UnidadResidencial {

    private int torres;
    private Boolean tieneCancha;
    private Boolean tienePiscina;

    private static volatile UnidadResidencial instance;

    private UnidadResidencial(int torres, Boolean tieneCancha, Boolean tienePiscina) {
        this.torres = torres;
        this.tieneCancha = tieneCancha;
        this.tienePiscina = tienePiscina;
    }

    public static UnidadResidencial getInstance(int torres, Boolean tieneCancha, Boolean tienePiscina) {
        if (instance == null)
            instance = new UnidadResidencial(torres, tieneCancha, tienePiscina);
        return instance;
    }

    @Override
    public String toString() {
        return "UnidadResidencial{" +
                "torres=" + torres +
                ", tieneCancha=" + tieneCancha +
                ", tienePiscina=" + tienePiscina +
                '}';
    }
}
